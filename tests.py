from solution import distinct


def test_1987():
    assert distinct(1987) == 2013


def test_2013():
    assert distinct(2013) == 2014


def test_2014():
    assert distinct(2014) == 2015


def test_2031():
    assert distinct(2031) == 2034
